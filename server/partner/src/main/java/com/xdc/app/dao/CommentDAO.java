package com.xdc.app.dao;

import com.xdc.app.pojo.Commet;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CommentDAO extends JpaRepository<Commet,Integer>{

}
