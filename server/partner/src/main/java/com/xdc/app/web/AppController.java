package com.xdc.app.web;

import com.xdc.app.dao.CommentDAO;
import com.xdc.app.dao.UserDAO;
import com.xdc.app.pojo.Commet;
import com.xdc.app.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.List;

@RestController
public class AppController {
    @Autowired
    CommentDAO commentDAO;
    @Autowired
    UserDAO userDAO;

    //登录
    @PostMapping("/login")
    public Object login(@RequestBody User user, HttpSession session){
        User u = userDAO.getUserByAccount(user.getAccount());

        if(u!=null && user.getPassword().equals(u.getPassword())){
            //设置时长
            session.setMaxInactiveInterval(60*60*24);
            session.setAttribute("user",u);
            return "ok";
        }

        return "fail";
    }

    //退出登录
    @RequestMapping("/logout")
    public String logout(HttpSession session){
        session.removeAttribute("user");
        session.invalidate();
        return "ok";
    }

    //注册
    @PostMapping("/signUp")
    public Object signUp(@RequestBody User user){
        System.out.println("signUp-"+user);
        user.setImage(1);
        userDAO.save(user);
        return "ok";
    }
    //获取评论列表
    @RequestMapping("/my")
    public Object listMy(){
        List<Commet> commetList = commentDAO.findAll();
        return commetList;
    }
    //发布评论
    @PostMapping("/my/add")
    public Object addMy(@RequestBody Commet commet){
        commentDAO.save(commet);
        return "ok";
    }
    //点赞
    @GetMapping("/my/addLove")
    public String addLove(@RequestParam(value = "count") Integer count){
        Commet commet = commentDAO.findOne(count);
        commet.setCounts(commet.getCounts()+1);
        commentDAO.save(commet);
        return "ok";
    }
}
