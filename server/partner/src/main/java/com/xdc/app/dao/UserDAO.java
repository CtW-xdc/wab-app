package com.xdc.app.dao;

import com.xdc.app.pojo.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserDAO extends JpaRepository<User,Integer>{
    public User getUserByAccount(String account);
}
