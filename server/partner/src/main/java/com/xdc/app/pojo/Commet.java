package com.xdc.app.pojo;

import javax.persistence.*;

@Entity
@Table(name = "comment")
public class Commet {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    private String comments;
    private String title;
    private int counts;

    @Override
    public String toString() {
        return "Commet{" +
                "id=" + id +
                ", comments='" + comments + '\'' +
                ", title='" + title + '\'' +
                ", counts='" + counts + '\'' +
                '}';
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public int getCounts() {
        return counts;
    }

    public void setCounts(int counts) {
        this.counts = counts;
    }
}
