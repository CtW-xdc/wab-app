package com.xdc.app.test;

import com.xdc.app.AppApplication;
import com.xdc.app.dao.CommentDAO;
import com.xdc.app.dao.UserDAO;
import com.xdc.app.pojo.Commet;
import com.xdc.app.pojo.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = AppApplication.class)
public class TestJPA {
    @Autowired
    CommentDAO commentDAO;
    @Autowired
    UserDAO userDAO;
    @Test
    public void testMyDAO(){
        Commet commet = commentDAO.findOne(1);
        System.out.println(commet);
    }

    @Test
    public void testUserDAO(){
        User user = userDAO.findOne(1);
        System.out.println(user);
    }
}
